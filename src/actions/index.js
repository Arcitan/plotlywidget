import storeData from '../data/wholeTableReduxStore.json'; 

const fetchData = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(storeData);
    }, 2000)
  });
};

export const DATA_LOADED = 'DATA_LOADED'; 
export const DATA_ERROR = 'DATA_ERROR'; 
export const TABLE_SELECTED = 'TABLE_SELECTED' 
export const X_SELECTED = 'X_SELECTED' 
export const Y_SELECTED = 'Y_SELECTED' 
export const TABLE_OPTIONS_BUILT = 'TABLE_OPTIONS_BUILT'
export const DATA_SELECTED = 'DATA_SELECTED' 

export function loadData() {
  return async function(dispatch) {
    try {
      const data = await fetchData();
      return dispatch({ type: DATA_LOADED, data });
    }
    catch (err) {
      return dispatch({ type: DATA_ERROR, err });
    }
  }
}; 

export function buildTableOptions(tableOptions ) {
  return {
    type: TABLE_OPTIONS_BUILT, 
    tableOptions 
  }
}

export function selectTable(id, columnOptions) {
  return {
    type: TABLE_SELECTED, 
    id,
    columnOptions,
  }
}

export function selectXData(label, data, id) {
  return {
    type: X_SELECTED, 
    label, 
    data, 
    id,
  }
}

export function selectYData(label, data, id) {
  return {
    type: Y_SELECTED, 
    label, 
    data, 
    id,
  }
}

export function setDataSelected(isDataSelected) {
  return {
    type: DATA_SELECTED, 
    isDataSelected, 
  }
}