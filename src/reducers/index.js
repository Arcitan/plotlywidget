import produce from 'immer'; 
import { 
  DATA_LOADED, 
  DATA_ERROR,
  TABLE_SELECTED, 
  X_SELECTED,
  Y_SELECTED,
  DATA_SELECTED,
  TABLE_OPTIONS_BUILT,
} from '../actions' 

const initialState = {
  data: [],
  layout: {},
  tableOptions: [],
};

const rootReducer = (state = initialState, action) => {
  return produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      case DATA_LOADED:
        draft.tableState = action.data; 
        draft.tableIds = Object.keys(action.data.tablesById);
        break; 
      case DATA_ERROR: 
        draft.loadError = true; 
        break; 
      case TABLE_OPTIONS_BUILT:
        draft.tableOptions = action.tableOptions;
        break; 
      case TABLE_SELECTED: 
        draft.selectedTable = action.id; 
        draft.columnOptions = action.columnOptions;
        break; 
      case X_SELECTED: 
        draft.xLabel = action.label;
        draft.xData = action.data;
        draft.xId = action.id; 
        break; 
      case Y_SELECTED: 
        draft.yLabel = action.label; 
        draft.yData = action.data;
        draft.yId = action.id; 
        break;
      case DATA_SELECTED: 
        draft.isDataSelected = action.isDataSelected; 
        break; 
    }
  });
}

export default rootReducer; 