import React, { Component } from 'react';
import { Dropdown } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { selectYData, setDataSelected } from '../actions';

class YSelector extends Component {
  constructor(props) {
    super(props)

    this.state = {};
  }

  /* Callback to handle when the user selects a new table in YSelector Dropdown component. Updates state with data in the selected column, and updates the setDataSelected bool. 
  { value } is the TableID field in the TableStore, passed by the Dropdown component in YSelector. */
  handleYChange = (e, { value }) => {
    const { tableState, selectedTable, xId, columnOptions } = this.props; 
    // If the selected column matches the table, then toggle setDataSelected accordingly. 
    if (xId && columnOptions.map(obj => obj.key).includes(value) &&
    columnOptions.map(obj => obj.key).includes(xId)) {
      console.log(true)
        this.props.setDataSelected(true); 
      } else {
        console.log(xId)
        console.log(false)
        this.props.setDataSelected(false); 
      }
    // Pull the data and column label from the table and add it to the state as the x-axis data/label. 
    const tableData = tableState.tablesById[selectedTable].data;
    const y = tableData[0] ?
      tableData.filter(row => Number(row[value])).map(row => Number(row[value])) :
      [];
    const yLabel = tableData[0] ? tableData[0][value] : '';
    this.props.selectYData(yLabel, y, value);
  }

  render() {
    const { selectedTable, columnOptions } = this.props;

    if (!selectedTable) {
      return <div />
    }
    return (
      <Dropdown
        onChange={this.handleYChange}
        options={columnOptions}
        selection
        placeholder='Select y-axis data...'
      />
    )
  }
}

function mapStateToProps(state) {
  return {
    selectedTable: state.selectedTable,
    columnOptions: state.columnOptions,
    tableState: state.tableState,
    xId: state.xId,
  }
}

export default connect(
  mapStateToProps,
  { selectYData, setDataSelected }
)(YSelector)