import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import Plot from 'react-plotly.js'; 

class TwoDPlot extends Component {
  constructor(props) {
    super(props)

    this.state = {};
  }

  /* Convert the data in the store into data used by Plotly Plot component */ 
  generateTraces = () => {
    const { xData, yData, } = this.props; 
    const trace1 = {
      x: xData, 
      y: yData, 
      mode: 'markers', 
      type: 'scatter', 
    }
    const data = [trace1];
    return data; 
  }

  /* Convert labels in the store into layout format used by Plotly Plot component */ 
  generateLayout = () => {
    const { xLabel, yLabel, selectedTable, tableState } = this.props; 
    const layout = (tableState && selectedTable) ? { 
      title: tableState.tablesMetaById[selectedTable].displayName,
      xaxis: {
        title: xLabel, 
      },
      yaxis: {
        title: yLabel,
      },
    } : {}
    return layout
  }

  render() { 
    const { isDataSelected } = this.props; 
    let data = []; 
    let layout = {}; 

    /* Only plot the data if the user has selected a table and columns that match that table. Otherwise, display an empty plot. */ 
    if (isDataSelected) {
      data = this.generateTraces(); 
      layout = this.generateLayout();
    }
    return (
      <div>
        <Plot
          data={data}
          layout={layout}
          style={{ width: '100%', height: '100%' }}
          /* TODO: Handle hover and click events */ 
          onHover={this.handleHover}
          onUnhover={this.handleUnhover}
          onClick={this.handleClick}
        />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    xData: state.xData, 
    yData: state.yData,
    xLabel: state.xLabel, 
    yLabel: state.yLabel, 
    selectedTable: state.selectedTable, 
    tableState: state.tableState, 
    isDataSelected: state.isDataSelected, 
  }
}

export default connect(mapStateToProps)(TwoDPlot)