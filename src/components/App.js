import React, {Component} from 'react';
import TwoDPlot from './TwoDPlot'; 
import { loadData, selectTable, buildTableOptions, } from '../actions'; 
import { connect } from 'react-redux';
import TableSelector from './TableSelector';
import { Loader, } from 'semantic-ui-react'; 

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {}; 
  }

  /* FIrst thing the component should do is do a fetch request to the back-end and get the table Redux store. */
  async componentDidMount() {
    await this.props.loadData();
    const tableState = this.props.tableState
    // Generate a table-options array-of-objects for the TableSelector component to select from. 
    const tableOptions = Object.keys(tableState.tablesById).map( id => ({
      key: id, 
      text: tableState.tablesMetaById[id].displayName, 
      value: id,
    })); 
    this.props.buildTableOptions(tableOptions); 
  }


  render() {
    /* Error and loading screens. */ 
    if (this.props.loadError) {
      return <div>couldn't load file</div>
    }
    if (!this.props.tableState) {
      return (
        <Loader active inline='centered'>Fetching tables</Loader>
      )
    }

    /* Default case -- data has been stored, now render the widget. */ 
    return (
      <div>
        <TableSelector/>
        <TwoDPlot/>
        <TwoDPlot/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    tableState: state.tableState, 
  }
}

export default connect(
  mapStateToProps, 
  { loadData, selectTable, buildTableOptions, }
)(App); 
