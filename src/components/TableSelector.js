import React, { Component } from 'react';
import { Dropdown, Grid, } from 'semantic-ui-react';
import { connect } from 'react-redux';
import YSelector from './YSelector';
import XSelector from './XSelector';
import { selectTable, setDataSelected } from '../actions';

class TableSelector extends Component {
  constructor(props) {
    super(props)

    this.state = {};
  }

  /* Callback to handle when the user selects a new table in TableSelector Dropdown component. Updates state with the table's column options, and updates the setDataSelected bool. 
  { value } is the TableID field in the TableStore, passed by the Dropdown component in TableSelector. */
  handleTableChange = (e, { value }) => {
    const { tableState, xId, yId } = this.props; 
    // Check if the x- and y- data sets match the current table that's selected, and toggle the setDataSelected bool accordingly if they match or not. 
    if (yId && xId && tableState.tablesById[value].data[0] && 
      (tableState.tablesById[value].data[0][xId] && tableState.tablesById[value].data[0][yId])) {
        this.props.setDataSelected(true); 
      } else {
        this.props.setDataSelected(false); 
      }
    // Create the column options (array of objects) used by the Dropdown. 
    const columnOptions = Object.keys( tableState.tablesById[value].data[0] ?
      tableState.tablesById[value].data[0] : [])
      .filter( id => (!(id === '_origin') && !(id === '_row_index')))
      .map( id => ({
      key: id, 
      text: tableState.tablesById[value].data[0][id], 
      value: id,
    })); 
    this.props.selectTable(value, columnOptions);
  }

  render() {
    const { tableOptions, selectedTable } = this.props;
    return (
      <Grid columns={3}>
        <Grid.Column>
          <Dropdown
            onChange={this.handleTableChange}
            options={tableOptions}
            placeholder="Select a data set..."
            selection
            value={selectedTable}
          />
        </Grid.Column>
        <Grid.Column>
          <XSelector />
        </Grid.Column>
        <Grid.Column>
          <YSelector />
        </Grid.Column>
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return {
    tableOptions: state.tableOptions,
    selectedTable: state.selectedTable,
    tableState: state.tableState, 
    xId: state.xId, 
    yId: state.yId, 
  }
}

export default connect(
  mapStateToProps,
  { selectTable, setDataSelected, }
)(TableSelector)