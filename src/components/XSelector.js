import React, { Component } from 'react';
import { Dropdown } from 'semantic-ui-react'
import { connect } from 'react-redux';
import { selectXData, setDataSelected } from '../actions';

class XSelector extends Component {
  constructor(props) {
    super(props)

    this.state = {};
  }

    /* Callback to handle when the user selects a new table in XSelector Dropdown component. Updates state with data in the selected column, and updates the setDataSelected bool. 
  { value } is the TableID field in the TableStore, passed by the Dropdown component in XSelector. */
  handleXChange = (e, { value }) => {
    const { tableState, selectedTable, yId, columnOptions } = this.props; 
    // If the selected column matches the table, then toggle setDataSelected accordingly. 
    if (yId && columnOptions.map(obj => obj.key).includes(value) 
    && columnOptions.map(obj => obj.key).includes(yId) ) {
        console.log(true)
        this.props.setDataSelected(true); 
      } else {
        console.log(false)
        this.props.setDataSelected(false); 
      }
    // Pull the data and column label from the table and add it to the state as the x-axis data/label. 
    const tableData = tableState.tablesById[selectedTable].data;
    const x = tableData[0] ? 
      tableData.filter(row => Number(row[value])).map( row => Number(row[value])):
      []; 
    const xLabel = tableData[0] ? tableData[0][value] : '';
    this.props.selectXData(xLabel, x, value); 
  }

  render() {
    const { selectedTable, columnOptions } = this.props;

    if (!selectedTable) {
      return <div />
    }
    return (
      <Dropdown 
        onChange={this.handleXChange}
        options={columnOptions}
        selection
        placeholder='Select x-axis data...'
      />
    )
  }
}

function mapStateToProps(state) {
  return {
    selectedTable: state.selectedTable,
    columnOptions: state.columnOptions,
    tableState: state.tableState, 
    yId: state.yId, 
  }
}

export default connect(
  mapStateToProps,
  { selectXData, setDataSelected } 
  )(XSelector)