import { configure } from 'enzyme'; 
import Adapter from 'enzyme-adapter-react-16'; 
import 'jest-canvas-mock'; 

window.URL.createObjectURL = jest.fn().mockImplementation( () => {}); 

configure({ adapter: new Adapter() }); 
